import { parse as dateFnsParse } from "date-fns";
import { parseTime } from "./utils";
import { parse as parseEntries } from "./entries";

const genders: { [key: string]: string } = {
  M: "male",
  F: "female",
  X: "mixed",
};

const parseGender = (value: string) => genders[value];

const parseAge = (value: string) => {
  const age = parseInt(value);
  return age === 0 ? undefined : age;
};

const parseDate = (dateString: string) => {
  const date = dateFnsParse(dateString, "MM/dd/yyyy", new Date());
  date.setHours(12);
  return date;
};

const strokes: { [key: string]: string } = {
  1: "free",
  2: "back",
  3: "breast",
  4: "fly",
  5: "medley",
  6: "free_relay",
  7: "medley_relay",
};

const parseStroke = (value: string) => strokes[value];

const courses: { [key: string]: string } = {
  S: "short",
  L: "long",
};

const parseCourse = (value: string) => courses[value];

const casts: { [key: string]: (value: string) => any } = {
  startDate: parseDate,
  endDate: parseDate,
  ageUpDate: parseDate,
  gender: parseGender,
  ageLower: parseAge,
  ageUpper: parseAge,
  distance: parseInt,
  stroke: parseStroke,
  course: parseCourse,
  qualificationTimeSC: parseTime,
  considerationTimeSC: parseTime,
  qualificationTimeLC: parseTime,
  considerationTimeLC: parseTime,
};

const hyv = {
  meetColumns: ["name", "startDate", "endDate", "ageUpDate", "course", "city"],
  eventColumns: [
    "eventNo",
    null,
    "gender",
    null,
    "ageLower",
    "ageUpper",
    "distance",
    "stroke",
    "qualificationTimeSC",
    "considerationTimeSC",
    null,
    null,
    "qualificationTimeLC",
    "considerationTimeLC",
  ],
  casts,
};

export async function parse(buffer: ArrayBuffer) {
  return parseEntries(buffer, hyv);
}
