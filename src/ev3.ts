import { parse as dateFnsParse } from "date-fns";
import { parseTime } from "./utils";
import { parse as parseEntries } from "./entries";

const genders: { [key: string]: string } = {
  M: "male",
  F: "female",
  W: "female",
  B: "male",
  G: "female",
  X: "mixed",
};

const parseGender = (value: string) => genders[value];

const parseAge = (value: string) => {
  const age = parseInt(value);
  if (age === 0 || age === 109) {
    return undefined;
  }
  return age;
};

const parseDate = (dateString: string) => {
  const date = dateFnsParse(dateString, "MM/dd/yyyy", new Date());
  date.setHours(12);
  return date;
};

const strokes: { [key: string]: string } = {
  A: "free",
  B: "back",
  C: "breast",
  D: "fly",
  E: "medley",
};

const parseStroke = (value: string) => strokes[value];

const courses: { [key: string]: string } = {
  S: "short",
  L: "long",
};

const parseCourse = (value: string) => courses[value];

const casts: { [key: string]: (value: string) => any } = {
  startDate: parseDate,
  endDate: parseDate,
  ageUpDate: parseDate,
  gender: parseGender,
  ageLower: parseAge,
  ageUpper: parseAge,
  distance: parseInt,
  stroke: parseStroke,
  course: parseCourse,
  qualificationTimeSC: parseTime,
  considerationTimeSC: parseTime,
  qualificationTimeLC: parseTime,
  considerationTimeLC: parseTime,
};

const ev3 = {
  meetColumns: [
    "name",
    "venue",
    "startDate",
    "endDate",
    "ageUpDate",
    "course",
    "city",
    null,
    null,
    null,

    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,

    null,
    null,
    null,
    null,
    null,
    null,
    "city",
    null,
    "postCode",
    null,
  ],
  eventColumns: [
    null,
    "eventNo",
    null,
    null,
    null,
    "gender",
    "ageLower",
    "ageUpper",
    "distance",
    "stroke",
    null,
    null,
    null,
    null,
    null,
    "qualificationTimeLC",
    "considerationTimeLC",
    "qualificationTimeSC",
    "considerationTimeSC",
    null,
    null,
    null,
    null,
    null,
    null,
    "course",
  ],
  casts,
};

export function parse(buffer: ArrayBuffer) {
  return parseEntries(buffer, ev3);
}
