export const set = (obj: Record<string, unknown>, path: string, value: any) => {
  const pathArray = path.match(/([^[.\]])+/g);
  if (pathArray == null) {
    obj = { ...obj, ...value };
    return obj;
  }
  pathArray.reduce((acc: any, key: string | number, i: number) => {
    if (key === "-1") {
      key = acc.length - 1;
    }
    if (acc[key] === undefined) acc[key] = {};
    if (i === pathArray.length - 1) acc[key] = { ...acc[key], ...value };
    return acc[key];
  }, obj);

  return obj;
};

export const append = (
  obj: Record<string, unknown>,
  path: string,
  value: any
) => {
  const pathArray = path.match(/([^[.\]])+/g);
  pathArray?.reduce((acc: any, key: string | number, i: number) => {
    if (key === "-1") {
      key = acc.length - 1;
    }
    if (i === pathArray.length - 1) {
      if (acc[key] === undefined) {
        acc[key] = [];
      }
      acc[key] = acc[key].concat(value);
    } else if (acc[key] === undefined) acc[key] = {};
    return acc[key];
  }, obj);
};

export function parseTime(time: string) {
  const regex = /^(((\d+):)?(\d+\.\d+))|(\d+)$/;
  const match = time.match(regex);
  if (!match) {
    return time;
  }
  if (match[5]) {
    return parseInt(match[5]);
  }
  const mins = parseInt(match[3]) || 0;
  const secs = parseFloat(match[4]);
  return Math.round((mins * 60 + secs + Number.EPSILON) * 100) / 100;
}
