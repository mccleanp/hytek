import { unzip } from "unzipit";

import { parse as parseBuffer } from "./parser";

export async function parse(buffer: ArrayBuffer) {
  const { entries } = await unzip(new Uint8Array(buffer));
  const entryName = ["hy3", "cl2", "sd3", "ev3", "hyv"]
    .map((ext) => {
      return Object.keys(entries).find((entryName) =>
        entryName.endsWith("." + ext)
      );
    })
    .find((entryName) => entryName != null);

  if (entryName == null) {
    throw new Error("No recognised files found in zip archive");
  }

  const data = await entries[entryName].arrayBuffer();
  return parseBuffer(data, entryName.slice(entryName.lastIndexOf(".") + 1));
}
