export interface TypeSpec {
  type?: string;
  enum?: string;
}

export interface ItemsSpec extends TypeSpec {
  len: number;
}

export interface FieldSpec extends TypeSpec {
  start: number;
  end?: number;
  len?: number;
  items?: ItemsSpec;
}

export interface ItemsSpec2 {
  start: number;
  len: number;
  count: number;
  properties: { [key: string]: FieldSpec };
}

export interface LineSpec {
  path: string;
  mode?: string;
  properties?: { [key: string]: FieldSpec };
  items?: ItemsSpec2;
}

export interface EnumSpec {
  [key: string]: { [key: string]: string };
}

export interface Error {
  line: number;
  message: string;
}

export type FileParser = (buffer: ArrayBuffer) => Promise<any[]>;

export interface FileParsers {
  [format: string]: FileParser;
}
