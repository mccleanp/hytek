import { parse as parseDate } from "date-fns";
import {
  EnumSpec,
  LineSpec,
  TypeSpec,
  Error,
  FieldSpec,
  ItemsSpec2,
} from "./types";
import { set, append, parseTime } from "./utils";

type Value = Date | string | number | boolean | Value[] | undefined | null;

interface TypeParsers {
  [key: string]: (value: string) => Value;
}

const typeParsers: TypeParsers = {
  name: (str: string) => str.split(", ").reverse(),
  date: (dateString: string) => {
    const date = parseDate(dateString, "MMddyyyy", new Date());
    date.setHours(12);
    return date;
  },
  time: parseTime,
  int: parseInt,
  bool: (s: string) => (s === "T" ? true : s === "F" ? false : null),
};

function parseType(value: string, fieldSpec: TypeSpec, enums: EnumSpec) {
  if (value === "") {
    return undefined;
  }

  if (fieldSpec.enum != null) {
    const e = enums[fieldSpec.enum];
    if (e[value] != null) {
      return e[value];
    }
    return value;
  }

  if (fieldSpec.type != null) {
    const typeParser = typeParsers[fieldSpec.type];
    if (typeParser != null) {
      return typeParser(value);
    }
  }

  return value;
}

function parseLineProperties(
  line: Uint8Array,
  properties: { [key: string]: FieldSpec },
  enums: EnumSpec,
  offset = 0
) {
  const obj: { [key: string]: Value } = {};
  for (const [field, fieldSpec] of Object.entries(properties)) {
    const { start, len, type = "string", items } = fieldSpec;
    let { end } = fieldSpec;
    if (end == null) {
      end = start + (len == null ? 0 : len - 1);
    }
    if (type === "array" && items != null) {
      obj[field] = decode(line, start - offset, end - offset)
        .match(new RegExp(".{1," + items.len + "}", "g"))
        ?.map((value) => value.trim())
        .map((value) => parseType(value, items, enums))
        .filter((value) => value != null);
    } else {
      const value = parseType(
        decode(line, start - offset, end - offset).trim(),
        fieldSpec,
        enums
      );
      if (value) {
        obj[field] = value;
      }
    }
  }
  return obj;
}

function parseLineItems(
  line: Uint8Array,
  items: ItemsSpec2,
  enums: EnumSpec,
  offset = 0
) {
  const results = [];
  const { start, len, count, properties } = items;
  for (let i = 0; i < count; i++) {
    const itemArray = line.slice(
      start + i * len - offset,
      start + i * len + len - offset
    );
    const item = parseLineProperties(itemArray, properties, enums);
    if (Object.keys(item).length > 0) {
      results.push(item);
    }
  }
  return results;
}

function parseLine(
  line: Uint8Array,
  lineSpec: LineSpec,
  enums: EnumSpec,
  offset?: number
) {
  if (lineSpec.properties != null) {
    return parseLineProperties(line, lineSpec.properties, enums, offset);
  } else if (lineSpec.items != null) {
    return parseLineItems(line, lineSpec.items, enums, offset);
  }
}

function computeChecksum(array: Uint8Array) {
  let evenSum = 0;
  let oddSum = 0;
  for (let i = 0; i < array.length; i++) {
    const code = array[i];
    if (i % 2 === 0) {
      evenSum += code;
    } else {
      oddSum += code;
    }
  }
  const sum = 205 + Math.floor((evenSum + oddSum * 2) / 21);

  const tens = Math.floor(sum / 10) % 10;
  const units = sum % 10;

  return units * 10 + tens;
}

function splitLines(buffer: ArrayBuffer) {
  const array = new Uint8Array(buffer);
  const { arrays } = array.reduce(
    (
      acc: { arrays: Uint8Array[]; start: number },
      cur: number,
      index: number
    ) => {
      const { arrays, start } = acc;
      if (array[index] === 10 && array[index - 1] === 13) {
        arrays.push(array.slice(start, index - 1));
        return { arrays, start: index + 1 };
      }
      return { arrays, start };
    },
    { arrays: [], start: 0 }
  );
  return arrays;
}

const decoder = new TextDecoder();

function decode(array: Uint8Array, start: number, end?: number) {
  return decoder.decode(array.slice(start, end == null ? start + 1 : end + 1));
}

interface FileSpec {
  kind: string;
  lineLength: number;
  records: { [key: string]: LineSpec };
  enums: EnumSpec;
  hasChecksum?: boolean;
  offset?: number;
}

export function parse(buffer: ArrayBuffer, spec: FileSpec) {
  const {
    kind,
    lineLength,
    hasChecksum = false,
    records,
    enums,
    offset,
  } = spec;

  const errors: Error[] = [];
  let output: any = { kind };

  const arrays = splitLines(new Uint8Array(buffer));
  arrays.forEach((array, line) => {
    if (array.length != lineLength) {
      errors.push({ line, message: "Illegal line length: " + array.length });
      return;
    }

    if (hasChecksum) {
      const checksum = parseInt(decode(array, lineLength - 2, lineLength - 1));
      if (checksum !== computeChecksum(array.slice(0, 128))) {
        errors.push({
          line,
          message:
            "Invalid checksum. Expected: " +
            computeChecksum(array.slice(0, lineLength - 2)) +
            ", Actual: " +
            checksum,
        });
        return;
      }
    }

    const lineType = decode(array, 0, 1);
    const lineSpec = records[lineType];
    if (lineSpec == null) {
      errors.push({ line, message: "Unsupported record type: " + lineType });
      return;
    }

    const { mode, path } = lineSpec;
    const record = parseLine(array, lineSpec, enums, offset);
    if (record) {
      if (mode === "append") {
        append(output, path, record);
      } else {
        output = set(output, path, record);
      }
    }
  });

  return [output, errors];
}
