import records from "./records";
import enums from "./enums";
import { parse as parseResults } from "../results";

const hy3 = {
  kind: "hy3",
  lineLength: 130,
  records,
  enums,
  hasChecksum: true,
};

export async function parse(buffer: ArrayBuffer) {
  return parseResults(buffer, hy3);
}
