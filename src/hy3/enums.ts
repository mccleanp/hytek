const enums = {
  stroke: {
    A: "FS",
    B: "BK",
    C: "BR",
    D: "BF",
    E: "IM",
  },
  sex: {
    M: "M",
    F: "F",
  },
  eventSex: {
    M: "M",
    F: "F",
    X: "X",
  },
  course: {
    S: "S",
    Y: "Y",
    L: "L",
  },
  resultType: {
    F: "FINAL",
    P: "PRELIM",
  },
  timeCode: {
    R: "NS",
    S: "SCR",
    F: "FS",
    Q: "DQ",
    D: "DNF",
  },
};

export default enums;
