// based on reverse engineering detailed in doc folder

import { LineSpec } from "../types";

const records: { [key: string]: LineSpec } = {
  A1: {
    path: ".",
    properties: {
      fileType: {
        start: 2,
        end: 3,
        type: "int",
      },
      fileTypeDescription: {
        start: 4,
        end: 28,
      },
      vendorName: {
        start: 29,
        end: 43,
      },
      softwareVersion: {
        start: 44,
        end: 57,
      },
      creationDate: {
        start: 58,
        end: 65,
        type: "date",
      },
      licensedTo: {
        start: 75,
        end: 127,
      },
    },
  },
  B1: {
    path: "meet",
    properties: {
      name: {
        start: 2,
        end: 46,
      },
      venue: {
        start: 47,
        end: 91,
      },
      startDate: {
        start: 92,
        end: 99,
        type: "date",
      },
      endDate: {
        start: 100,
        end: 107,
        type: "date",
      },
      ageUpDate: {
        start: 108,
        end: 115,
        type: "date",
      },
      elevation: {
        start: 116,
        end: 120,
        type: "int",
      },
    },
  },
  B2: {
    path: "meet",
    properties: {
      type: {
        start: 96,
        end: 97,
      },
      course: {
        start: 98,
        enum: "course",
      },
    },
  },
  C1: {
    path: "meet.teams",
    mode: "append",
    properties: {
      abbrev: {
        start: 2,
        end: 6,
      },
      name: {
        start: 7,
        end: 36,
      },
      shortName: {
        start: 37,
        end: 52,
      },
      lsc: {
        start: 53,
        end: 54,
      },
      type: {
        start: 119,
        end: 121,
      },
    },
  },
  C2: {
    path: "meet.teams[-1].info",
    properties: {
      mailTo: {
        start: 2,
        end: 31,
      },
      address: {
        start: 32,
        end: 61,
      },
      city: {
        start: 62,
        end: 91,
      },
      state: {
        start: 92,
        end: 94,
      },
      postCode: {
        start: 94,
        end: 103,
      },
      country: {
        start: 104,
        end: 106,
      },
      teamRegistration: {
        start: 108,
        end: 111,
      },
    },
  },
  C3: {
    path: "meet.teams[-1].info",
    properties: {
      phoneDay: {
        start: 32,
        end: 51,
      },
      phoneEvening: {
        start: 52,
        end: 71,
      },
      fax: {
        start: 72,
        end: 91,
      },
      email: {
        start: 92,
        end: 127,
      },
    },
  },
  D1: {
    path: "meet.teams[-1].swimmers",
    mode: "append",
    properties: {
      sex: {
        start: 2,
        enum: "sex",
      },
      eventSwimmerId: {
        start: 3,
        end: 7,
      },
      lastName: {
        start: 8,
        end: 27,
      },
      firstName: {
        start: 28,
        end: 47,
      },
      knownAs: {
        start: 48,
        end: 67,
      },
      middleInitial: {
        start: 68,
      },
      uss: {
        start: 69,
        end: 80,
      },
      teamId: {
        start: 83,
        end: 87,
      },
      dob: {
        start: 88,
        end: 95,
        type: "date",
      },
      age: {
        start: 97,
        end: 98,
        type: "int",
      },
    },
  },
  E1: {
    path: "meet.teams[-1].swimmers[-1].entries",
    mode: "append",
    properties: {
      sex: {
        start: 2,
        enum: "sex",
      },
      eventSwimmerId: {
        start: 3,
        end: 7,
      },
      swimmerAbbrev: {
        start: 8,
        end: 12,
      },
      distance: {
        start: 17,
        end: 20,
        type: "int",
      },
      stroke: {
        start: 21,
        enum: "stroke",
      },
      ageLower: {
        start: 22,
        end: 24,
        type: "int",
      },
      ageUpper: {
        start: 25,
        end: 27,
        type: "int",
      },
      fee: {
        start: 32,
        end: 37,
      },
      eventNo: {
        start: 38,
        end: 41,
      },
      seedTimeConversion1: {
        start: 42,
        end: 49,
        type: "time",
      },
      seedCourseConversion1: {
        start: 50,
        enum: "course",
      },
      seedTime1: {
        start: 52,
        end: 58,
        type: "time",
      },
      seedCourse1: {
        start: 59,
        enum: "course",
      },
      seedTimeConversion2: {
        start: 60,
        end: 67,
        type: "time",
      },
      seedCourseConversion2: {
        start: 68,
        enum: "course",
      },
      seedTime2: {
        start: 69,
        end: 75,
        type: "time",
      },
      seedCourse2: {
        start: 76,
        enum: "course",
      },
    },
  },
  E2: {
    path: "meet.teams[-1].swimmers[-1].entries[-1].results",
    mode: "append",
    properties: {
      resultType: {
        start: 2,
        enum: "resultType",
      },
      time: {
        start: 3,
        end: 10,
        type: "time",
      },
      course: {
        start: 11,
      },
      timeCode: {
        start: 12,
        enum: "timeCode",
      },
      heat: {
        start: 20,
        end: 22,
        type: "int",
      },
      lane: {
        start: 23,
        end: 25,
        type: "int",
      },
      heatPlace: {
        start: 26,
        end: 28,
        type: "int",
      },
      place: {
        start: 29,
        end: 32,
        type: "int",
      },
      date: {
        start: 87,
        end: 94,
        type: "date",
      },
      dayOfEvent: {
        start: 102,
        end: 109,
      },
    },
  },
  G1Z: {
    path: "meet.teams[-1].swimmers[-1].entries[-1].results[-1].splits",
    mode: "append",
    items: {
      start: 2,
      len: 11,
      count: 11,
      properties: {
        resultType: {
          start: 0,
          enum: "resultType",
        },
        length: {
          start: 1,
          end: 2,
          type: "int",
        },
        time: {
          start: 3,
          end: 10,
          type: "time",
        },
      },
    },
  },
};

export default records;
