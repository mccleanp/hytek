import { parse as parseCsv } from "csv-parse/sync";

export async function parse(
  buffer: ArrayBuffer,
  { meetColumns, eventColumns, casts }: any
) {
  const errors: Error[] = [];

  const cast = (value: string, context: any) => {
    if (context.header) return value;
    if (value === "") return undefined;
    if (casts[context.column]) return casts[context.column](value);
    return value;
  };

  const text = new TextDecoder().decode(buffer);

  const meet = parseCsv(text, {
    delimiter: ";",
    to_line: 1,
    columns: meetColumns,
    relax_column_count_more: true,
    cast,
  })[0];

  meet.events = parseCsv(text, {
    delimiter: ";",
    from_line: 2,
    relax_column_count_more: true,
    columns: eventColumns,
    cast,
  });

  return [meet, errors];
}
