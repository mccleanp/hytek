import records from "./records";
import enums from "./enums";
import { parse as parseResults } from "../results";

const sdifv3 = {
  kind: "sdifv3",
  lineLength: 160,
  records,
  enums,
  offset: 1,
};

export async function parse(buffer: ArrayBuffer) {
  return parseResults(buffer, sdifv3);
}
