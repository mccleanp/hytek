const enums = {
  stroke: {
    1: "FS",
    2: "BK",
    3: "BR",
    4: "BF",
    5: "IM",
    6: "FS_RELAY",
    7: "MEDLEY_RELAY",
  },
  sex: {
    M: "M",
    F: "F",
  },
  eventSex: {
    M: "M",
    F: "F",
    X: "X",
  },
  course: {
    S: "S",
    Y: "Y",
    L: "L",
    1: "S",
    2: "Y",
    3: "L",
  },
};

export default enums;
