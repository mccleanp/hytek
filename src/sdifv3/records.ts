// based on specification at https://www.usms.org/admin/sdifv3f.txt

import { LineSpec } from "../types";

const records: { [key: string]: LineSpec } = {
  A0: {
    path: ".",
    properties: {
      orgCode: {
        start: 3,
      },
      sdifVersion: {
        start: 4,
        len: 8,
      },
      fileCode: {
        start: 12,
        len: 2,
      },
      softwareName: {
        start: 44,
        len: 20,
      },
      softwareVersion: {
        start: 64,
        len: 10,
      },
      contactName: {
        start: 74,
        len: 20,
      },
      contactPhone: {
        start: 94,
        len: 12,
      },
      creationDate: {
        start: 106,
        len: 8,
        type: "date",
      },
      submittedByLsc: {
        start: 156,
        len: 2,
      },
    },
  },
  B1: {
    path: "meet",
    properties: {
      orgCode: {
        start: 3,
      },
      name: {
        start: 12,
        len: 30,
      },
      address1: {
        start: 42,
        len: 22,
      },
      address2: {
        start: 64,
        len: 22,
      },
      city: {
        start: 86,
        len: 20,
      },
      state: {
        start: 106,
        len: 2,
      },
      postCode: {
        start: 108,
        len: 10,
      },
      country: {
        start: 118,
        len: 3,
      },
      meetCode: {
        start: 121,
      },
      startDate: {
        start: 122,
        len: 8,
        type: "date",
      },
      endDate: {
        start: 130,
        len: 8,
        type: "date",
      },
      altitude: {
        start: 138,
        len: 4,
        type: "int",
      },
      course: {
        start: 150,
        enum: "course",
      },
    },
  },
  B2: {
    path: "meet.hosts",
    mode: "append",
    properties: {
      orgCode: {
        start: 3,
      },
      name: {
        start: 12,
        len: 30,
      },
      address1: {
        start: 42,
        len: 22,
      },
      address2: {
        start: 64,
        len: 22,
      },
      city: {
        start: 86,
        len: 20,
      },
      state: {
        start: 106,
        len: 2,
      },
      postCode: {
        start: 108,
        len: 10,
      },
      country: {
        start: 118,
        len: 3,
      },
      phone: {
        start: 121,
        len: 12,
      },
    },
  },
  C1: {
    path: "meet.teams",
    mode: "append",
    properties: {
      orgCode: {
        start: 3,
      },
      region: {
        start: 4,
        len: 2,
      },
      teamCode: {
        start: 12,
        len: 6,
      },
      fullName: {
        start: 18,
        len: 30,
      },
      abbrevName: {
        start: 48,
        len: 16,
      },
      address1: {
        start: 64,
        len: 22,
      },
      address2: {
        start: 86,
        len: 22,
      },
      city: {
        start: 108,
        len: 20,
      },
      postCode: {
        start: 130,
        len: 10,
      },
      country: {
        start: 140,
        len: 3,
      },
      teamCodeChksum: {
        start: 150,
      },
    },
  },
  C2: {
    path: "meet.teams[-1].info",
    properties: {},
  },
  D0: {
    path: "meet.teams[-1].results",
    mode: "append",
    properties: {
      orgCode: {
        start: 3,
      },
      swimmerName: {
        start: 12,
        len: 28,
        type: "name",
      },
      uss: {
        start: 40,
        len: 12,
      },
      attachCode: {
        start: 52,
      },
      citizenCode: {
        start: 53,
        len: 3,
      },
      dob: {
        start: 56,
        len: 8,
        type: "date",
      },
      ageClass: {
        start: 64,
        len: 2,
      },
      sex: {
        start: 66,
        enum: "sex",
      },
      eventSex: {
        start: 67,
        enum: "eventSex",
      },
      distance: {
        start: 68,
        len: 4,
        type: "int",
      },
      stroke: {
        start: 72,
        enum: "stroke",
      },
      eventNo: {
        start: 73,
        len: 4,
      },
      ageLower: {
        start: 77,
        len: 2,
        type: "int",
      },
      ageUpper: {
        start: 79,
        len: 2,
        type: "int",
      },
      date: {
        start: 81,
        len: 8,
        type: "date",
      },
      seedTime: {
        start: 89,
        len: 8,
        type: "time",
      },
      seedCourse: {
        start: 97,
        enum: "course",
      },
      prelimsTime: {
        start: 98,
        len: 8,
        type: "time",
      },
      prelimsCourse: {
        start: 106,
        enum: "course",
      },
      swimoffTime: {
        start: 107,
        len: 8,
        type: "time",
      },
      swimoffCourse: {
        start: 115,
        enum: "course",
      },
      finalsTime: {
        start: 116,
        len: 8,
        type: "time",
      },
      finalsCourse: {
        start: 124,
        enum: "course",
      },
      prelimsHeat: {
        start: 125,
        len: 2,
        type: "int",
      },
      prelimsLane: {
        start: 127,
        len: 2,
        type: "int",
      },
      finalsHeat: {
        start: 129,
        len: 2,
        type: "int",
      },
      finalsLane: {
        start: 131,
        len: 2,
        type: "int",
      },
      prelimsPlace: {
        start: 133,
        len: 3,
        type: "int",
      },
      finalsPlace: {
        start: 136,
        len: 3,
        type: "int",
      },
      points: {
        start: 139,
        len: 4,
        type: "int",
      },
      eventTimeClass: {
        start: 143,
        len: 2,
      },
      flightStatus: {
        start: 145,
      },
    },
  },
  D3: {
    path: "meet.teams[-1].swimmers",
    mode: "append",
    properties: {
      uss: {
        start: 3,
        len: 14,
      },
      preferredName: {
        start: 17,
        len: 15,
      },
      ethnicity: {
        start: 32,
        len: 2,
      },
      juniorHighSchool: {
        start: 34,
        type: "bool",
      },
      seniorHighSchool: {
        start: 35,
        type: "bool",
      },
      ymca: {
        start: 36,
        type: "bool",
      },
      college: {
        start: 37,
        type: "bool",
      },
      summerSwimLeague: {
        start: 38,
        type: "bool",
      },
      master: {
        start: 39,
        type: "bool",
      },
      disabledSportsOrg: {
        start: 40,
        type: "bool",
      },
      waterPolo: {
        start: 41,
        type: "bool",
      },
    },
  },
  E0: {
    path: "meet.teams[-1].results",
    mode: "append",
    properties: {
      orgCode: {
        start: 3,
      },
      relayTeamNameSuffix: {
        start: 12,
      },
      teamCode: {
        start: 13,
        len: 6,
      },
      resultCount: {
        start: 19,
        len: 2,
        type: "int",
      },
      eventSex: {
        start: 21,
        enum: "eventSex",
      },
      distance: {
        start: 22,
        len: 4,
        type: "int",
      },
      stroke: {
        start: 26,
        enum: "stroke",
      },
      eventNo: {
        start: 27,
        len: 4,
      },
      ageLower: {
        start: 31,
        len: 2,
        type: "int",
      },
      ageUpper: {
        start: 33,
        len: 2,
        type: "int",
      },
      totalAge: {
        start: 35,
        len: 3,
        type: "int",
      },
      date: {
        start: 38,
        len: 8,
        type: "date",
      },
      seedTime: {
        start: 46,
        len: 8,
        type: "time",
      },
      seedCourse: {
        start: 54,
        enum: "course",
      },
      prelimsTime: {
        start: 55,
        len: 8,
        type: "time",
      },
      prelimsCourse: {
        start: 63,
        enum: "course",
      },
      swimoffTime: {
        start: 64,
        len: 8,
        type: "time",
      },
      swimoffCourse: {
        start: 72,
        enum: "course",
      },
      finalsTime: {
        start: 73,
        len: 8,
        type: "time",
      },
      finalsTourse: {
        start: 81,
        enum: "course",
      },
      prelimsHeat: {
        start: 82,
        len: 2,
        type: "int",
      },
      prelimsLane: {
        start: 84,
        len: 2,
        type: "int",
      },
      finalsHeat: {
        start: 86,
        len: 2,
        type: "int",
      },
      finalsLane: {
        start: 88,
        len: 2,
        type: "int",
      },
      prelimsPlace: {
        start: 90,
        len: 3,
        type: "int",
      },
      finalsPlace: {
        start: 93,
        len: 3,
        type: "int",
      },
      points: {
        start: 96,
        len: 4,
        type: "int",
      },
      eventTimeClass: {
        start: 100,
        len: 2,
      },
    },
  },
  F0: {
    path: "meet.teams[-1].results[-1].names",
    mode: "append",
    properties: {
      orgCode: {
        start: 3,
      },
      teamCode: {
        start: 16,
        len: 6,
      },
      relayTeamNameSuffix: {
        start: 22,
      },
      swimmerName: {
        start: 23,
        len: 28,
        type: "name",
      },
      uss: {
        start: 51,
        len: 12,
      },
      citizenCode: {
        start: 63,
        len: 3,
      },
      swimmersDob: {
        start: 66,
        len: 8,
        type: "date",
      },
      ageClass: {
        start: 74,
        len: 2,
      },
      sex: {
        start: 76,
        enum: "sex",
      },
      prelimOrderCode: {
        start: 77,
      },
      swimoffOrderCode: {
        start: 78,
      },
      finalsOrderCode: {
        start: 79,
      },
      time: {
        start: 80,
        len: 8,
        type: "time",
      },
      course: {
        start: 88,
        enum: "course",
      },
      takeOffTime: {
        start: 89,
        len: 4,
        type: "time",
      },
      preferredName: {
        start: 107,
        len: 15,
      },
    },
  },
  G0: {
    path: "meet.teams[-1].results[-1].splits",
    mode: "append",
    properties: {
      orgCode: {
        start: 3,
      },
      name: {
        start: 16,
        len: 28,
        type: "name",
      },
      uss: {
        start: 44,
        len: 12,
      },
      seqNo: {
        start: 56,
        type: "int",
      },
      splitTotal: {
        start: 57,
        len: 2,
        type: "int",
      },
      distance: {
        start: 59,
        len: 4,
        type: "int",
      },
      splitCode: {
        start: 63,
      },
      splits: {
        start: 64,
        len: 80,
        type: "array",
        items: {
          len: 8,
          type: "time",
        },
      },
      prelimFinalsCode: {
        start: 144,
      },
    },
  },
  Z0: {
    path: "term",
    properties: {},
  },
};

export default records;
