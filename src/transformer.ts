function shallowEquals(newObj: any, prevObj: any) {
  for (const key in newObj) {
    const a = newObj[key] instanceof Date ? newObj[key].getTime() : newObj[key];
    const b =
      prevObj[key] instanceof Date ? prevObj[key].getTime() : prevObj[key];
    if (a !== b) {
      console.log(key, a, b);
      return false;
    }
  }
  return true;
}

function findOrCreateEvent(events: any[], event: any) {
  const found = events.find((e: any) => e.eventNo === event.eventNo);
  if (found) {
    if (!shallowEquals(event, found)) {
      console.log(found, event);
      throw new Error("Conflicting event details between results");
    }
    return found;
  }
  events.push(event);
  return event;
}

function transformCl2(team: any, course: any, meet: any) {
  team.results.forEach((result: any) => {
    const {
      eventNo,
      distance,
      eventSex,
      ageLower,
      ageUpper,
      date,
      stroke,
      seedTime,
      prelimsTime,
      prelimsCourse,
      prelimsHeat,
      prelimsLane,
      prelimsPlace,
      finalsTime,
      finalsCourse,
      finalsHeat,
      finalsLane,
      finalsPlace,
      swimmerName,
      uss,
      dob,
      sex,
    } = result;

    if (!["BF", "BK", "BR", "FS"].includes(stroke)) {
      // unsupported stroke
      return;
    }

    if (
      (prelimsCourse && prelimsCourse !== course) ||
      (finalsCourse && finalsCourse != course)
    ) {
      throw new Error("Inconsistent course between meet, prelims and finals");
    }

    const event = findOrCreateEvent(meet.events, {
      eventNo,
      course,
      distance,
      ageLower: ageLower == "UN" ? undefined : ageLower,
      ageUpper: ageUpper == "OV" ? undefined : ageUpper,
      stroke,
    });
    event.gender = eventSex;
    event.entries = event.entries || [];
    event.prelims = event.prelims || [];
    event.finals = event.finals || [];

    const swimmer = {
      swimmerId: uss,
      teamId: team.teamCode.replace(team.region, ""),
      team: team.fullName,
      firstName: swimmerName[0],
      lastName: swimmerName[1],
      dob,
      gender: sex,
    };

    event.entries.push({
      ...swimmer,
      time: seedTime,
    });

    if (prelimsTime) {
      event.prelims.push({
        ...swimmer,
        date,
        time: typeof prelimsTime === "number" ? prelimsTime : undefined,
        timeCode: typeof prelimsTime !== "number" ? prelimsTime : undefined,
        heat: prelimsHeat,
        lane: prelimsLane,
        place: prelimsPlace,
      });
    }

    if (finalsTime) {
      event.finals.push({
        ...swimmer,
        date,
        time: typeof finalsTime === "number" ? finalsTime : undefined,
        timeCode: typeof finalsTime !== "number" ? finalsTime : undefined,
        heat: finalsHeat,
        lane: finalsLane,
        place: finalsPlace,
      });
    }
  });
}

function transformHy3(team: any, course: any, meet: any) {
  team.swimmers.forEach((swimmer: any) => {
    const { firstName, lastName, sex: gender, uss, dob } = swimmer;

    if (swimmer.entries) {
      swimmer.entries.forEach((entry: any) => {
        const {
          eventNo,
          distance,
          sex: eventSex,
          ageLower,
          ageUpper,
          stroke,
          seedTime1: seedTime,
        } = entry;

        if (!["BF", "BK", "BR", "FS"].includes(stroke)) {
          // unsupported stroke
          return;
        }

        const event = findOrCreateEvent(meet.events, {
          eventNo,
          course,
          distance,
          ageLower: ageLower == 0 ? undefined : ageLower,
          ageUpper: ageUpper == 109 ? undefined : ageUpper,
          stroke,
        });
        event.gender = eventSex;

        event.entries = event.entries || [];
        event.prelims = event.prelims || [];
        event.finals = event.finals || [];

        const swimmer = {
          swimmerId: uss,
          teamId: team.abbrev.slice(0, 4),
          team: team.name,
          firstName,
          lastName,
          dob,
          gender,
        };

        event.entries.push({
          ...swimmer,
          time: seedTime,
        });

        entry.results.forEach((result: any) => {
          if (result.resultType === "PRELIM") {
            event.prelims.push({
              ...swimmer,
              date: result.date,
              time: result.time,
              timeCode: result.timeCode,
              heat: result.heat,
              lane: result.lane,
              place: result.place,
            });
          }
          if (result.resultType === "FINAL") {
            event.finals.push({
              ...swimmer,
              date: result.date,
              time: result.time,
              timeCode: result.timeCode,
              heat: result.heat,
              lane: result.lane,
              place: result.place,
            });
          }
        });
      });
    }
  });
}

export function transformer(input: any) {
  const { name, venue, city, postCode, startDate, endDate, course } =
    input.meet;

  const meet: any = {
    name,
    venue,
    city,
    postCode,
    startDate,
    endDate,
    course,
    events: [],
  };

  input.meet.teams.forEach((team: any) => {
    if (team.results) {
      transformCl2(team, course, meet);
    } else if (team.swimmers) {
      transformHy3(team, course, meet);
    }
  });

  meet.events.sort((a: any, b: any) => {
    return a.eventNo.localeCompare(b.eventNo, undefined, { numeric: true });
  });

  meet.events.forEach((event: any) => {
    event.entries.sort((a: any, b: any) => {
      return a.time - b.time;
    });
    event.prelims.sort((a: any, b: any) => {
      if (a.timeCode != null) {
        return -1;
      }
      if (b.timeCode != null) {
        return 1;
      }
      return a.time - b.time;
    });
    event.finals.sort((a: any, b: any) => {
      if (a.timeCode != null) {
        return -1;
      }
      if (b.timeCode != null) {
        return 1;
      }
      return a.time - b.time;
    });
  });

  return meet;
}
