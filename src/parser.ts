import { parse as parseSdifv3 } from "./sdifv3";
import { parse as parseHy3 } from "./hy3";
import { parse as parseHyv } from "./hyv";
import { parse as parseEv3 } from "./ev3";
import { parse as parseZip } from "./zip";
import { FileParsers } from "./types";
import { transformer } from "./transformer";

const fileParsers: FileParsers = {
  zip: parseZip,
  hy3: parseHy3,
  cl2: parseSdifv3,
  sd3: parseSdifv3,
  hyv: parseHyv,
  ev3: parseEv3,
};

export async function parse(
  buffer: ArrayBuffer,
  format: string,
  { transform } = { transform: false }
) {
  const parseFn = fileParsers[format];
  if (parseFn == null) {
    throw new Error("Unsupported file format: " + format);
  }
  const [data, errors] = await parseFn(buffer);
  if (transform) {
    return [transformer(data), errors];
  }
  return [data, errors];
}
