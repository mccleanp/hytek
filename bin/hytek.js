#!/bin/env node

const {readFile} = require('fs/promises');
const path = require('path');
const { program } = require('commander');
const { parse, transform } = require('../dist/index.js');
const sizeof = require('firestore-size');

program
  .name('hytek')
  .description('CLI to parse hytek files to JSON')
  .argument('<file>', 'path to file to parse')
  .option('-t, --transform')
  .action(async (filename, options) => {
    const ext = path.extname(filename).slice(1);
    const buffer = await readFile(filename);
    const [data, errors] = await parse(buffer, ext, options);
    console.log(JSON.stringify(data, null, 2));
    if (errors.length > 0) {
      errors.forEach(({line, message}) => console.warn(`${line}: ${message}`));
    }
    console.warn(`Size: `+sizeof(data))
  })

program.parseAsync();